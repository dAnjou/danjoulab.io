from mistune import HTMLRenderer


class TocItem:
    def __init__(self, level, anchor, text):
        self.level = level
        self.anchor = anchor
        self.text = text

    def render_heading(self):
        return (
            f'<h{self.level} id="toc-{self.anchor}">'
            f'{self.text} '
            f'<a href="#toc-{self.anchor}" class="toc"></a>\n'
            f'</h{self.level}>'
        )

    def render_anchor(self):
        return f'<a href="#toc-{self.anchor}" class="toc">{self.text}</a>'


class Toc:
    def __init__(self, toc_item_class=None):
        self._toc_item_class = toc_item_class or TocItem
        self._toc_items = []
        self._toc_level = 1
        self._toc = ""
        self._toc_count = 1

    def push_toc_item(self, level, text):
        item = self._toc_item_class(level, self._toc_count, text)
        self._toc_items.append(item)
        self._toc_count += 1
        return item

    def render(self):
        self._toc += "<ul>\n"
        self._render()
        self._toc += "</ul>\n"
        return self._toc

    def _render(self):
        if not self._toc_items:
            self._toc += "</ul>\n" * (self._toc_level - 1)
            return
        item = self._toc_items[0]
        if item.level > self._toc_level:
            self._toc_level += 1
            self._toc += "<ul>\n"
        elif item.level < self._toc_level:
            self._toc_level -= 1
            self._toc += "</ul>\n"
        else:
            self._toc += "<li>%s</li>\n" % item.render_anchor()
            self._toc_items.pop(0)
        self._render()


class TocRenderer(HTMLRenderer):
    def __init__(self, **kwargs):
        super(TocRenderer, self).__init__(**kwargs)
        self.toc = Toc()

    def header(self, text, level, raw=None):
        item = self.toc.push_toc_item(level, text)
        return item.render_heading()
