import gzip
import shutil


def compress(path):
    if path.suffix.lower() not in {
        ".html",
        ".js",
        ".css",
        ".jpg",
        ".eot",
        ".ico",
        ".svg",
        ".ttf",
        ".woff",
        ".woff2"
    }:
        return
    with path.open('rb') as original:
        with gzip.open(path.with_suffix(path.suffix + ".gz"), 'wb') as compressed:
            shutil.copyfileobj(original, compressed)
