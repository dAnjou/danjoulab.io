from pathlib import Path

import mistune
import requests
from flask import Flask, render_template
from flask_frozen import Freezer
from markupsafe import Markup
from werkzeug.utils import secure_filename

from compress import compress
from markdown import TocRenderer

app = Flask(__name__)
app.config['FREEZER_DESTINATION'] = 'public'
freezer = Freezer(app)


@app.template_filter('markdown')
def markdown_filter(text):
    renderer = TocRenderer(escape=False)
    markdown = mistune.create_markdown(renderer=renderer)
    return Markup(markdown(text).replace("[TOC]", renderer.toc.render()))


@app.template_filter('svg')
def svg_filter(url, **options):
    data = requests.get(url, **options)
    return Markup(data.text)


@app.cli.command()
def freeze():
    freezer.freeze()
    for path, _, filenames in Path(app.config['FREEZER_DESTINATION']).walk():
        for filename in filenames:
            compress(path / filename)


@app.cli.command()
def run():
    freezer.run()


@freezer.register_generator
def view_generator():
    for template_path in app.jinja_env.list_templates():
        try:
            path = Path(template_path).relative_to("content").stem
            yield 'view', {'path': path}
        except ValueError:
            pass


@app.route("/", defaults={'path': 'root'})
@app.route("/<path:path>/")
def view(path):
    template = str(Path("content", secure_filename(path) + ".html"))
    return render_template(template)
