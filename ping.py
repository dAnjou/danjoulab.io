import sys
import requests

urls = [
    "https://maxludwig.de",
    "https://danjou.de",
    "https://maxludwig.dev",
    "https://danjou.dev",
]


def cli():
    exit_code = 0

    for url in urls:
        try:
            response = requests.get(url=url)
            response.raise_for_status()
            print(f"{url}: {response.status_code}")
        except Exception as e:
            exit_code = 1
            print(f"{url}: {e}")

    sys.exit(exit_code)


if __name__ == '__main__':
    cli()
